module.exports = {
  title: '雾寒VuePress博客',
  description: '雾寒VuePress博客',
  base: '/',
  themeConfig: {
    nav: [
      {
        text: '首页',
        link: '/',
      },
      {
        text: '标签',
        link: '/Tags/',
      },
      {
        text: '摘抄',
        link:'/Favor/',
        items: [
          { text: '红色记忆', link: '/Favor/Red_Memory' },
          { text: '文学摘抄', link: '/Favor/Literature' },
          ],
      },
      {
        text: '开发',
        link: '/Develop/',
        items: [
          { text: 'Docker', link: '/Develop/Docker' },
          { text: 'VuePress', link: '/Develop/VuePress' },
          ],
      },
      {
        text: '游戏',
        link: '/Game/',
      },
    ],
    smoothScroll: true, //流畅滚动
    globalPagination: {
      prevText: '上一页', // Text for previous links.
      nextText: '下一页', // Text for next links.
      lengthPerPage: '2', // Maximum number of posts per page.
      layout: 'Pagination', // Layout for pagination page
    }
  },
  
};
