---
title: VuePress 的各种部署
---

# 记录VuePress 的各种Serverless部署

## VuePress on [Vercel](www.vercel.com)

### 1. 代码拉取
::: warning
经测试，vercel暂时无法拉取除github,gitlab,bitbucket外git网站的代码，如gitee,coding等。
:::

### 2. 配置生产和开发配置（Build & Development Settings）
#### BUILD COMMAND:
```sh
yarn build or npm run build
```

#### OUTPUT DIRECTORY:
```sh
默认: ./.vuepress/dist
```

#### INSTALL COMMAND:
```sh
yarn add -D vuepress or npm install -D vuepress
```

### 3. 等待Deployments
