FROM  node:lts
WORKDIR /vuepress
COPY . /vuepress
RUN yarn && yarn global add vuepress
VOLUME [ "/vuepress" ]
EXPOSE 8080
CMD [ "/bin/bash" ]